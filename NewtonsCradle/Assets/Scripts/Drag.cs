﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour, IDragHandler
{
    [SerializeField]
    private RectTransform ball;

    private Quaternion zeroRotation = new Quaternion(0, 0, 0, 0);

    public void Update()
    {
        ball.rotation = zeroRotation;
    }

    public void OnDrag(PointerEventData eventData)
    {
        ball.position = eventData.position;
    }
}
